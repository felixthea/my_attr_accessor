class Object
  def new_attr_accessor(*inst_vars)
    inst_vars.each do |inst_var|
      self.send(:define_method, inst_var) { self.instance_variable_get(conversion(inst_var)) }
      self.send(:define_method, conversion2(inst_var)) { |value| self.instance_variable_set(conversion(inst_var), value) }
    end
  end

  def conversion(inst_var)
    inst_var.to_s.split("").unshift("@").join("")
  end

  def conversion2(inst_var)
    (inst_var.to_s+"=").to_sym
  end
end



# self.send(:define_method, inst_var) { self.instance_variable_get(conversion(inst_var)) }
# self.send(:define_method, inst_var)